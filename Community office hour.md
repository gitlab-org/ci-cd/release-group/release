Hi :wave: 

We at the ~"group::release" team are hosting community office hours some time soon :tada: 

You can find the full agenda below.

## How to join

The meeting is run as a Zoom call. See the zoom link below.

**Please note** the meeting will be recorded and posted to GitLab Unfiltered on Youtube.

## Meeting details
- When: **Date time and timezone**
- Duration: 50 minutes
- Zoom link: **ZOOM LINK HERE**
- Meeting recording: **recording on youtube**
- Meetup link: **link on meetup**
- Host: **someone on the team**
- Presenter: **someone on the team**

## Agenda
1. Introductions - GitLab team members
1. Introductions - Community members on this call
1. Main topic: **Main topic**
1. Questions / Future topics
1. Next meetings will be detailed in [the Epic](https://gitlab.com/groups/gitlab-org/-/epics/4512)


## Meeting prep
- [ ] `PM and EM`, find somebody on the team to host the meeting and somebody to present the main topic, assign them to this issue.
- [ ] `Host`, coordinate with `Presenter` on time and date of the office hour. This time should be convinient for you two, everyone else can join if they can, but it's usually not possible to choose time convenient for everyone.
- [ ] `Presenter` Choose the main topic for this office hour. You can look at previous office hour's retrospectives for inspiration.
- [ ] `Host` Create an agenda.
- [ ] `Host` Create the meeting in google calendar.
- [ ] `Host` Coordinate with dev-evangelist team to create slot in office hours and create Meetup event.
- [ ] `Host` double check that zoom link and the agenda document are accessible.
- [ ] `Host` Promote on social.
- [ ] `Host` Invite current milestone contributors and others.

## Post meeting recap
- [ ] `Host and Presenter` Post meeting highlights.
- [ ] `Host` Create a retrospective thread on this issue, invite the team and community contributor to give their feedback.
- [ ] `Host` Capture any todos from the meeting notes and the retrospective.
