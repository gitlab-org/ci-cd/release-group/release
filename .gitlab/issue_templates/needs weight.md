### Overview

This is a [Needs Weight/Refinement](https://about.gitlab.com/handbook/engineering/development/ops/release/planning/#needs-weightrefinement-issue) issue
that will be assigned every milestone to refine and weigh issues that are _to be scheduled in the next 2-3 milestones_.
The groupings are \~4 issues per person.

### Ask

Please refine the issue and assign a weight to it. To see what kind of refinements have been or should be made,
you can copy and paste the following template into an issue comment:

```markdown
## Refinement Progress

<!-- If a checkbox is not relevant in the issue, please remove it. -->

Needs Weight/Refinement issue: <link-to-the-issue>

- [ ] This issue describes a problem to solve, and it's confirmed.
- [ ] This issue describes a proposal that outlines a solution for the problem to solve.
- [ ] This issue requires input from the Product Designer to determine a UX proposal.
- [ ] This issue includes a UX proposal that is up-to-date and has been validated.
- [ ] This issue requires a technical proposal (e.g. link to the code to be changed), and it's added and up-to-date.
- [ ] This issue could affect application performance, and the concern is explained in the issue description.
- [ ] This issue could affect application security, and the concern is explained in the issue description.
- [ ] This issue is the smallest iteration possible and doesn't require further break down.
- [ ] This issue is linked to related, blocking and blocked issues, and it's up-to-date.
- [ ] This issue is labeled correctly.
- [ ] This issue requires PoC, prototype or Technical Evaluation due to its high complexity, and it's already finished.
- [ ] This issue describes a list of tasks or expected merge requests, therefore the weight is set and ~"needs weight" label is removed.
      If both ~backend and ~frontend work are required, add `~backend-weight::*` and `~frontend-weight::*` labels as well.
- [ ] Finally, add ~"workflow::ready for development" label to this issue.
```

You can always ping the other members to get an additional input for the refinement. If you can't finish the refinement due to
outstanding concerns or uncertainties, please reach out `@nicolewilliams`.

### Timing

- Due date: 2nd week of the assigned milestone

- Time Box: \~4hrs

## Backend

### Vladimir
- [ ] 

### Shinya
- [ ] 

### Bala
- [ ] 

### Allen
- [ ] 

### Ali
- [ ] 

### Ahmed
- [ ]

## Frontend 

### Andrew
- [ ] 

### Andrei
- [ ] 

/label ~"group::release" ~"section::ops" ~"devops::release"
