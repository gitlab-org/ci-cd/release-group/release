
Weekly summary of the top priorities for the Release team.

### :rocket: Milestone Deliverables
| ~Deliverable | % Complete |
| ------ | ------ |
| 🟢🟡🔴 XX.X Milestone |  |

### :spy: [Product](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=start_date_asc&layout=MONTHS&timeframe_range_type=CURRENT_YEAR&label_name%5B%5D=devops%3A%3Arelease&label_name%5B%5D=Roadmap&progress=WEIGHT&show_progress=false&show_milestones=false&milestones_type=ALL) focus

#### Insights

#### Epics we are working on

#### Key feature status 
| Feature | Category | Status |
| ------ | ------ | ------ |
|  |  |   |

### :vertical_traffic_light: : Reliability health check 
- 🟢🟡🔴  **Infradev** - [count: 0, past due: 0](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::release&label_name[]=infradev) 
- 🟢🟡🔴 **Security (past SLO)** - [count S1/S2:  0, count S3: 0](https://app.periscopedata.com/app/gitlab/913607/Past-Due-Security-Issues-Development?stage=Release&widget=13349870&udv=1580145)
- 🟢🟡🔴 **Regressions** [count: 0](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::release&label_name[]=regression)
- 🟢🟡🔴  **Corrective Action** [count: 0](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::release&label_name[]=corrective+action)
- 🟢🟡🔴  **Error budget Remaining** [X min](https://dashboards.gitlab.net/d/stage-groups-release/stage-groups-release-group-dashboard?orgId=1) (:arrow_up: :arrow_down: from X min). 

**Notes:** 

### :dart: OKRs
- [FYXX-QX OKRs for Release team](https://app.ally.io/teams/44661/objectives?tab=0&time_period_id=155987&viewId=212690) 

**Notes:** 

### :muscle: Team
- 
