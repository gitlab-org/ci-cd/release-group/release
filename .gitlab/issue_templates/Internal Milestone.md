# Internal Milestone <!-- Add Milestone --> Review & Discussion :rocket: 

**<!-- Add Milestone--> Milestone**: 2023-MM-18 to 2023-MM-17

**<!-- Add Milestone--> Theme**:

[Plan Board](https://gitlab.com/groups/gitlab-org/-/boards/1489550)

[Build Board](https://gitlab.com/groups/gitlab-org/-/boards/1489558)

  
## Capacity :package:

[Team Metrics Dashboard](https://app.periscopedata.com/app/gitlab/681347/Development-Embedded-Dashboard?filter=team_group+eq+release)
[Team Say/Do Ratio Dashboard](https://app.periscopedata.com/app/gitlab/658030/Say-Do-Ratios?filters=devops_stage+eq+release)

**Average deliverables (with weight):** <!-- Add average of Say/Do with weights here -->

<!--
For each section below, please add top issues in the following format:

* [Issue Title](https://gitlab.com/gitlab-org/gitlab/issues/xxxxx)

If no issues, please use 

* No issues currently listed :muscle:

--> 

## Deliverables :fox:

This includes all planned issues, including User Experience, Bugs, Security, Technical Debt, UX debt, Usability etc. 
 
Top priority deliverables: 

<!-- Add Issues Here --> 

~"Release::P1"

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |

~"Release::P2"

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |

~"Release::P3"

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |

**Total weight: XX**

## Technical Research :sleuth_or_spy: 

<!-- Add Issues Here --> 

Needs Weight Issue:

## Design and User Research 🌀

<!-- Add Issues Here --> 

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |

**Total weight: XX**

## Docs :writing_hand:

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |

**Total weight: XX**

## OKR Alignment :dart:
<!-- Add Issues Here --> 

## Planning Tasks
* [ ] EM update capacity section
* [ ] PM highlights work for Deliverables, User Experience, Bug Fixes, Security Fixes, Technical Debt sections
* [ ] UX highlights opportunities for User Experience and Research
* [ ] PM describes how this milestone aligns with our team's OKRs
* [ ] Team reviews prioritized issues for readiness/feasibility
