Welcome to the Release team! This issue is intended to be a companion to your Gitlab onboarding issue, and to help you get familiar with your specific product group. This is **your** issue, so feel free to make changes, add comments, or keep track of anything you want to review with your manager. 

**Your onboarding buddy is `@HANDLE`**

#### Tasks for the Buddy

- [ ] Complete buddy tasks in the team member onboarding issue
- [ ] Add your new teammate to the recurring team meetings
- [ ] Add your new teammate to the Release team Calendar 
- [ ] Add your new teammate to the relevant Slack channels

## Your Onboarding Tasks

This list is not exhaustive, and the onboarding buddy is encouraged to add any missing information (or either the engineer or buddy can open MRs directly in the handbook!)

### Get Familiar with the Release Stage

- [ ] Read over the [Release Team Handbook page](https://about.gitlab.com/handbook/engineering/development/ops/release/) and [planning page](https://about.gitlab.com/handbook/engineering/development/ops/release/planning/)
- [ ] Read and familiarize yourself with the [Delivery Direction page ](https://about.gitlab.com/direction/delivery/)
- [ ] Review product documentation for [Release feature categories](https://about.gitlab.com/direction/ops/#release)

### Working Asynchronously

- [ ] Watch [Bias toward Async training with Sid](https://youtu.be/_okcPC9YucA)
- [ ] Read [When to use asynchronous instead of synchronous communication](https://about.gitlab.com/company/culture/all-remote/asynchronous/#when-to-use-asynchronous-instead-of-synchronous-communication)
- [ ] Read [Create mental space](https://about.gitlab.com/company/culture/all-remote/asynchronous/#create-mental-space)
- [ ] Read [Understanding Low context communication](https://about.gitlab.com/company/culture/all-remote/effective-communication/#understanding-low-context-communication)

We use the following issues/boards to work asynchronously throughout the milestone, it is a good idea to familiarize yourself with each of these.
- [ ] Review [Build board](https://gitlab.com/groups/gitlab-org/-/boards/1489558?label_name[]=group%3A%3Arelease&milestone_title=<CURRENT MILESTONE>)
- [ ] Review [<CURRENT MILESTONE> planning issue](ISSUE URL)
- [ ] Review [<CURRENT MILESTONE> needs weight issue](ISSUE URL)

### Meet some team members
- [ ] Schedule a coffee chat with the team:
  - [ ] Team member 1
  - [ ] Team member 2
  - [ ] Team member 3
  - [ ] Team member 4
  - [ ] Team member 5
  - [ ] Team member 6
  - ...

### Getting started
  - [ ] Assign yourself to the issue linked here, and complete them at your own pace:
    - ISSUE 1
    - ISSUE 2
    - ISSUE 3
